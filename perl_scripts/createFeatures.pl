#!/usr/bin/perl
use lib "C:\\Strawberry\\perl\\site\\lib";
use lib "C:\\Strawberry\\perl\\vendor\\lib";
use strict;
use warnings;
use Data::Dumper;
use feature 'say';
use lib qw(..);
use JSON qw( );
#configuration
my $evaluationId = 3;
my $movieId = 2;
my $personId = 1;
my $idId = 0;

sub getMovieObject{
   my @args = @_;
   my $filename = "db/$args[0]";
   my $json_text = do {
      open(my $json_fh, "<:encoding(UTF-8)", $filename)
         or die("Can't open $filename: $!\n");
      local $/;
      <$json_fh>
   };

   my $json = JSON->new;
   my $data = $json->decode($json_text);
   return $data;
}

sub parseArray{
   my @param = $_[0];
   my $index = 0;
  if (scalar @_ > 1) {
      $index = $_[1];
   }
   if(@param > 0) {
      return parseObject($param[0][$index]);
   }
   return 0;
}

sub parseObject{
   my $param = $_[0];
   
   if($param) {
      return $param->{id}
   }
   return 0;
}



if(scalar @ARGV < 1) {
  die "At least 1 files";
}

print "File to trasform: $ARGV[0]\n";
my $file = $ARGV[0];

my @nameArr = split(/\\/, $file);
my $outputFile = pop @nameArr;
$outputFile = "featureset_$outputFile"; 

open(my $input, '<:encoding(UTF-8)', $file) or die "Could not open file '$file' $!";
open(my $output, '>', $outputFile) or die "Could not open file '$outputFile' $!";
my @rows = ();
 
my %scalerValues = ();
my $fieldNumber = 0;
while (my $row = <$input>) {
  chomp $row;
  my @cols = ();
  my @fields = split ";" , $row;
  my $movie = getMovieObject($fields[$movieId]);
  push(@cols, $fields[$idId]);  push(@cols, $fields[$personId]);
  push(@cols, $fields[$movieId]);
  push(@cols, $fields[$evaluationId]);
  push(@cols, $movie->{budget}); 
  push(@cols, $movie->{popularity});
  push(@cols, $movie->{vote_average});
  push(@cols, parseObject($movie->{belongs_to_collection}));
  push(@cols, parseArray($movie->{genres}));
  push(@cols, parseArray($movie->{genres},1));
  push(@rows, \@cols);
  $fieldNumber = scalar @cols - 4;
}
my $index = 0;
for(my $i = 0; $i < $fieldNumber; $i++) {
   $scalerValues{4 + $i} = { min => 0, max => 0 };
}

foreach my $row (@rows) {
   my $i = 0;
    foreach my $cell (@$row) {
      if($i > 3) {
         if($scalerValues{$i}->{min} > $cell) {
            $scalerValues{$i}->{min} = $cell;         }
         if($scalerValues{$i}->{max} < $cell) {
            $scalerValues{$i}->{max} = $cell;
         }
      }
      $i++;
    }
}

foreach my $row (@rows) {
   my $i = 0;
    foreach my $cell (@$row) {
      if($i != 0) {
         print $output ";";
      }        if($i > 3) {  
         print $output ($cell - $scalerValues{$i}->{min}) / ($scalerValues{$i}->{max}-$scalerValues{$i}->{min});
      } else {
         print $output $cell;      }
      $i++;
    }
    print $output "\n";
}







