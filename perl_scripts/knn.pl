#!/usr/bin/perl
use strict;
use warnings;

use Data::Dumper;
use feature 'say';

sub getData{
  my $hash = shift;
  my $fileName = shift;
  open(my $fh, '<:encoding(UTF-8)', $fileName) or die "Could not open file '$fileName' $!";
  while (my $row = <$fh>) {
    chomp $row;
    my @fields = split ";" , $row;
    my @features = @fields;
    splice @features, 0, 4;
    $hash->{$fields[1]}{$fields[0]} = { evaluation => $fields[3], features =>  \@features } ;

  }
  close $fh;
}

sub getDistance{
  my $features1 = shift;
  my $features2 = shift;
  my $i = 0;
  my  $sum = 0;
  foreach my $elem (@$features1) {
    $sum += ($elem-@$features2[$i])*($elem-@$features2[$i]);
    $i++;
  }
  return $sum;
}

sub getNN{
  my @actualMovieFeature = shift;
  my $trainHash = shift;
  my @keys = keys %$trainHash;
  my $minDistance = getDistance(@actualMovieFeature, $trainHash->{$keys[0]}->{features});
  my $bestN = $keys[0];
    while(@keys) {
      my $movieId = pop(@keys);
      my $distance = getDistance(@actualMovieFeature, $trainHash->{$movieId}->{features});
      if( $distance < $minDistance) {
          $minDistance = $distance;
          $bestN = $movieId;
      }
  }
  return $trainHash->{$bestN}->{evaluation};
}

sub getNN2{
  my @actualMovieFeature = shift;
  my $trainHash = shift;
  my @keys = keys %$trainHash;
  my $minDistance = getDistance(@actualMovieFeature, $trainHash->{$keys[0]}->{features});
  my $bestN = $keys[0];
    while(@keys) {
      my $movieId = pop(@keys);
      my $distance = getDistance(@actualMovieFeature, $trainHash->{$movieId}->{features});
      if( $distance < $minDistance) {
          $minDistance = $distance;
          $bestN = $movieId;
      }
  }
  return $trainHash->{$bestN}->{evaluation};
}

sub setNullEvaluations{
  my $train = shift;
  my $task = shift;
  my @keys = keys %$task;
  while (@keys) {
    my $actualKey = pop(@keys);
    my $hash = $task->{$actualKey};
    my @actualKeys = keys %$hash;
    while(@actualKeys) {
      my $movieId = pop(@actualKeys);
      $task->{$actualKey}->{$movieId}->{evaluation} = getNN($task->{$actualKey}->{$movieId}->{features}, $train->{$actualKey}); 
    }
  }
  #foreach all persons in task
    #foreach all movies in person
      #count Distance for all movies in train{person}
      #choose the best
      #set elevation
    
}

my %trainData = ();
my %taskData = ();

if(scalar @ARGV < 2) {
  die "At least 2 files";
}

print "Train file: $ARGV[0]\nTask file: $ARGV[1]\n";
my $trainFile = $ARGV[0];
my $taskFile = $ARGV[1];

my @nameArr = split(/\\/, $taskFile);
my $outputFile = pop @nameArr;
$outputFile = "submission.csv"; 

getData(\%trainData, $trainFile);
getData(\%taskData, $taskFile);

getDistance($trainData{913}{28148}->{features}, $trainData{913}{28160}->{features});
setNullEvaluations(\%trainData, \%taskData);


open(my $input, '<:encoding(UTF-8)', $taskFile) or die "Could not open file '$taskFile' $!";
open(my $output, '>', $outputFile) or die "Could not open file '$outputFile' $!";
while (my $row = <$input>) {
  chomp $row;
  my @fields = split ";" , $row;
  my $evaluation = $taskData{$fields[1]}{$fields[0]}->{evaluation};
  print $output "$fields[0];$fields[1];$fields[2];$evaluation\n";
}

print "File created: $outputFile";
