dir=$1
apikey='14b0d1523ee139d75db33e9df18f62a2'

if ! [[ -d $dir ]]
then
    mkdir "$dir"
fi

pushd "$dir"

cat ../movie.csv | while IFS=''  read -r i || [[ -n $i ]]
do
    db_id=$(echo $i | perl -pe 's/^.*;([^;]*);.*$/\1/g')
    farm_id=$(echo $i | perl -pe 's/^(.*);([^;]*);.*$/\1/g')

    curl  "https://api.themoviedb.org/3/movie/${db_id}?api_key=$apikey" > "$farm_id"
    
done

popd
